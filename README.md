# BotaMsg v2.5

A script that helps you to send message with your telegram bot to groups in telegram

## Steps to use this script

Clone the script: 
- !# git clone https://gitlab.com/cyberknight777/botamsg
- !# cd botamsg 
- !# Install jq , git & curl
- !# ./botamsg
- Create a bot & get bot token from https://t.me/botfather . (Example Here: https://youtu.be/MZixi8oIdaA) 
- Next add your bot in the group(s) of your likings.
- Get the chat Id by the group(s) by typing /id . (That group should have a group management bot which has /id command available to use.)
- Copy the bot token and chat Id(s) and...

use in this script as you like. :) 

> Available in: 

+ [gitlab](https://gitlab.com/cyberknight777/botamsg)
+ [knight-repo](https://gitlab.com/cyberknight777/knight-repo)


## Issues

- Unknown, fill out a Issue page if you found any bug
